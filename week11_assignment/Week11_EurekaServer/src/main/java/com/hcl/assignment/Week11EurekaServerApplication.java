package com.hcl.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class Week11EurekaServerApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(Week11EurekaServerApplication.class, args);
		System.out.println("server running on 8761 port.....");
	}

}
