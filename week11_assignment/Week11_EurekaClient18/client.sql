create database  client;
use client;
CREATE TABLE Book(
  id        int not null, 
  title    varchar(25) not null, 
  isbn           int not NULL, 
  PRIMARY KEY(id)
);
insert into Book values(1,"Java",145415);

select * from Book;

create table user (
   id int,
   name varchar(45) not null,
   password  varchar(45) not null,
   primary key(id));
insert into user values(1,"admin","123456");

select * from user;

create table liked(id int not null ,
name varchar(10) not null,
isbn int not null,
primary key(id));

insert into liked values(3,"java",67890);

select * from liked;

create table Readed(id int not null,name varchar(25) not null,isbn int not null,primary key(id));
insert into Readed values(2,"DataStructure",452451);
select * from Readed;
