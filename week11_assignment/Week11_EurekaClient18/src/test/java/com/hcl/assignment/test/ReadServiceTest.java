package com.hcl.assignment.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.hcl.assignment.bean.Read;
import com.hcl.assignment.dao.ReadDao;
import com.hcl.assignment.service.ReadService;

@SpringBootTest
class ReadServiceTest {
	@InjectMocks
	ReadService readService;
	@Mock
	ReadDao readDao;

	@Test
	void testReadLaterBook() {
		// fail("Not yet implemented");
		Read read = new Read();
		readService.readLaterBook(read);
		verify(readDao, times(1)).save(read);

	}

	@Test
	void testGetAllReadLaterbooks() {
//		fail("Not yet implemented");
		List<Read> list = new ArrayList<Read>();
		Read b1 = new Read();
		Read b2 = new Read();
		list.add(b1);
		list.add(b2);
		when(readDao.findAll()).thenReturn(list);
		List<Read> bookList = readService.getAllReadLaterbooks();
		assertEquals(2, bookList.size());
		verify(readDao, times(1)).findAll();
	}

	@Test
	void testDeletereadLaterBook() {
		// fail("Not yet implemented");
		Read read = new Read(5, "b4", 6899);
		readDao.save(read);
		readDao.deleteById(read.getId());
		Optional optional = readDao.findById(read.getId());
		assertEquals(Optional.empty(), optional);
	}

}
