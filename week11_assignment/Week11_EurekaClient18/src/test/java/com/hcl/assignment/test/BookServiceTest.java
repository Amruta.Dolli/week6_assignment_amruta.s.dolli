package com.hcl.assignment.test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.verification.VerificationMode;
import org.springframework.boot.test.context.SpringBootTest;
import static org.mockito.Mockito.times;

import com.hcl.assignment.bean.Book;
import com.hcl.assignment.dao.BookDao;
import com.hcl.assignment.service.BookService;

@SpringBootTest
class BookServiceTest {

	@InjectMocks
	BookService bookService;
	@Mock
	BookDao bookDao;

	@Test
	void testGetAllbooks() {
		// fail("Not yet implemented");
		List<Book> list = new ArrayList<Book>();
		Book b1 = new Book();
		Book b2 = new Book();
		Book b3 = new Book();
		list.add(b1);
		list.add(b2);
		list.add(b3);
		when(bookDao.findAll()).thenReturn(list);
		List<Book> bookList = bookService.getAllbooks();
		assertEquals(3, bookList.size());
		verify(bookDao, times(1)).findAll();

	}

	@Test
	void testStoreBook() {
		// fail("Not yet implemented");
		Book book = new Book();
		bookService.storeBook(book);
		verify(bookDao, times(1)).save(book);

	}

	@Test
	void testDeleteBook() {

		Book book = new Book(5, "b4", 6899);
		bookDao.save(book);
		bookDao.deleteById(book.getId());
		Optional optional = bookDao.findById(book.getId());
		assertEquals(Optional.empty(), optional);

	}

//	@Test
//	void testUpdateBook() {
////		//fail("Not yet implemented");
//		Book book = new Book(1, ".net", 4567);
//		when(bookDao.findById(book.getId())).thenReturn(Optional.of(book));
//		bookService.updateBook(book);
//		verify(bookDao, times(1)).saveAndFlush(book);
//
//	}

}
