package com.hcl.assignment.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.springframework.boot.test.context.SpringBootTest;


import com.hcl.assignment.bean.Like;

import com.hcl.assignment.dao.LikeDao;

import com.hcl.assignment.service.LikeService;

@SpringBootTest
class LikeServiceTest {
	@InjectMocks
LikeService likeService;
	@Mock
	LikeDao likeDao;

	

	@Test
	void testLikeBook() {
		//fail("Not yet implemented");
	Like like = new Like();
		likeService.likeBook(like);
		verify(likeDao, times(1)).save(like);
		
	}

	@Test
	void testGetAllLikedbooks() {
		//fail("Not yet implemented");
		List<Like> list = new ArrayList<Like>();
		Like b1 = new Like();
		Like b2 = new Like();
		list.add(b1);
		list.add(b2);
		when(likeDao.findAll()).thenReturn(list);
		List<Like> bookList = likeService.getAllLikedbooks();
		assertEquals(2, bookList.size());
		verify(likeDao, times(1)).findAll();

	}

	@Test
	void testDeleteLikedBook() {
	//	fail("Not yet implemented");
		Like like = new Like(5, "b4", 6899);
		likeDao.save(like);
		likeDao.deleteById(like.getId());
		Optional optional = likeDao.findById(like.getId());
		assertEquals(Optional.empty(), optional);
	}

}
