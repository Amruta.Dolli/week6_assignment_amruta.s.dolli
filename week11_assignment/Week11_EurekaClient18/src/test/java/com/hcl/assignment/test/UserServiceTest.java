package com.hcl.assignment.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import static org.mockito.Mockito.times;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.context.SpringBootTest;

import com.google.common.base.Optional;
import com.hcl.assignment.bean.User;
import com.hcl.assignment.dao.UserDao;
import com.hcl.assignment.service.UserService;

@SpringBootTest
class UserServiceTest {

	@InjectMocks
	UserService userService;

	@Mock
	UserDao userDao;

	@Test
	void testGetAllusers() {
		// fail("Not yet implemented");
		List<User> list = new ArrayList<User>();
		User u1 = new User();
		User u2 = new User();
		User u3 = new User();
		list.add(u1);

		list.add(u2);
		list.add(u3);
		when(userDao.findAll()).thenReturn(list);
		List<User> userList = userService.getAllusers();
		assertEquals(3, userList.size());
		verify(userDao, times(1)).findAll();
	}

	@Test
	void testRegister() {
		// fail("Not yet implemented");
		User user = new User();
		userService.register(user);
		verify(userDao, times(1)).save(user);
	}

	// @Test
	// void testUserLogin() {
	// fail("Not yet implemented");
	//
	// when(userDao.getById(1)).thenReturn(new User(1,"Amruta"));
	// }

//	@Test
//	void testUserLogout() {
//		fail("Not yet implemented");
//	}

}
