package com.hcl.assignment.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Readed")
public class Read {
	@Id
	@Column
	private int id;
	@Column
	private String name;
	@Column
	private int isbn;

	public Read() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Read(int id, String name, int isbn) {
		super();
		this.id = id;
		this.name = name;
		this.isbn = isbn;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIsbn() {
		return isbn;
	}

	public void setIsbn(int isbn) {
		this.isbn = isbn;
	}

	@Override
	public String toString() {
		return "Like [id=" + id + ", name=" + name + ", isbn=" + isbn + "]";
	}

}
