package com.hcl.assignment.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.assignment.bean.Read;

@Repository
public interface ReadDao extends JpaRepository<Read, Integer>{

}
