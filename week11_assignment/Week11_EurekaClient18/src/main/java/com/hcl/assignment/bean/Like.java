package com.hcl.assignment.bean;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "liked")
public class Like {
	@Id
	private int id;
	private String name;
	private int isbn;

	public Like() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Like(int id, String name, int isbn) {
		super();
		this.id = id;
		this.name = name;
		this.isbn = isbn;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIsbn() {
		return isbn;
	}

	public void setIsbn(int isbn) {
		this.isbn = isbn;
	}

	@Override
	public String toString() {
		return "Like [id=" + id + ", name=" + name + ", isbn=" + isbn + "]";
	}

}
