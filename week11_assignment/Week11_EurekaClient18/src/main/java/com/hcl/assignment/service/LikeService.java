package com.hcl.assignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.assignment.bean.Like;
import com.hcl.assignment.dao.LikeDao;

@Service
public class LikeService {

	@Autowired
	LikeDao likeDao;

	public String likeBook(Like like) {
		if (likeDao.existsById(like.getId())) {
			return "id must be unique";
		} else {
			likeDao.save(like);
			return "book liked succesfully";
		}

	}

	public List<Like> getAllLikedbooks() {
		return likeDao.findAll();
	}

	public String deleteLikedBook(int id) {
		if (!likeDao.existsById(id)) {
			return "book details not found in likesection";
		} else {
			likeDao.deleteById(id);
			return "Book deleted succesfully from likesection";
		}
	}
}
