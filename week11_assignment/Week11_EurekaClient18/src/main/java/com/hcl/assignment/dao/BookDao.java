package com.hcl.assignment.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.assignment.bean.Book;

public interface BookDao extends JpaRepository<Book, Integer> {

}
