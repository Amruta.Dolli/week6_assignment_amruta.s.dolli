package com.hcl.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.hcl.assignment.bean.Like;
import com.hcl.assignment.service.LikeService;

@RestController
@RequestMapping("/like")
public class LikeController {

	@Autowired
	LikeService likeService;

	@GetMapping(value = "getAllLikedBook", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Like> getAllLikedBooks() {
		return likeService.getAllLikedbooks();
	}
	@PostMapping(value = "likeBook", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String likeBook(@RequestBody Like like) {
		return likeService.likeBook(like);
	}

	@RequestMapping(value = "deleteFromLike/{id}", method = RequestMethod.DELETE)
	public String deleteLike(@PathVariable("id") int id) {
		return likeService.deleteLikedBook(id);
	}
}
