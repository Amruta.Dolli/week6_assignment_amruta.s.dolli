package com.hcl.assignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.assignment.bean.Read;

import com.hcl.assignment.dao.ReadDao;

@Service
public class ReadService {
	@Autowired
	ReadDao readDao;

	public String readLaterBook(Read read) {
		if (readDao.existsById(read.getId())) {
			return "id must be unique";
		} else {
			readDao.save(read);
			return "book added succesfully readLaterSection";
		}

	}

	public List<Read> getAllReadLaterbooks() {
		return readDao.findAll();
	}

	public String deletereadLaterBook(int id) {
		if (!readDao.existsById(id)) {
			return "book details not found in readLatersection";
		} else {
			readDao.deleteById(id);
			return "Book deleted succesfully from readLatersection";
		}
	}

}
