package com.hcl.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.assignment.bean.Read;
import com.hcl.assignment.service.ReadService;

@RestController
@RequestMapping("/read")
public class ReadController {
	@Autowired
ReadService readService;

	@GetMapping(value = "getAllReadLaterBook", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Read> getAllReadLaterBooks() {
		return readService.getAllReadLaterbooks();
	}
	@PostMapping(value = "ReadLaterBook", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeReadLaterBook(@RequestBody Read read) {
		return readService.readLaterBook(read);
	}

	@RequestMapping(value = "deleteFromReadLater/{id}", method = RequestMethod.DELETE)
	public String deleteBookFromReadLater(@PathVariable("id") int id) {
		return readService.deletereadLaterBook(id);
	}
}
