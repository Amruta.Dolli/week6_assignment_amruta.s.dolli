package com.hcl.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages = "com.hcl.assignment")
@EnableEurekaClient
@EnableSwagger2
@EnableJpaRepositories(basePackages = "com.hcl.assignment.dao")
public class Week11EurekaClient18Application {

	public static void main(String[] args) {
		SpringApplication.run(Week11EurekaClient18Application.class, args);
		System.out.println("server running on port no 8181......");
	}
	
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.hcl.assignment")).build();
	}

}
