package com.hcl.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.assignment.bean.User;
import com.hcl.assignment.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	
	UserService userService;

	@PostMapping(value = "loginUser", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String login(@RequestBody User user) {
		return userService.userLogin(user);
	}

	@PostMapping(value = "logoutAdmin", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String logout(@RequestBody User user) {
		return userService.userLogout(user);
	}

	@GetMapping(value = "getAllusers", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<User> getAllUsers() {
		return userService.getAllusers();
	}

	@PostMapping(value = "registerUser", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String register(@RequestBody User user) {
		return userService.register(user);
	}
}
