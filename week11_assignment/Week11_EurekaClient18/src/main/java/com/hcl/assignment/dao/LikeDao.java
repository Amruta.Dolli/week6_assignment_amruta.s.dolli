package com.hcl.assignment.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.assignment.bean.Like;

@Repository
public interface LikeDao extends JpaRepository<Like, Integer> {

}
