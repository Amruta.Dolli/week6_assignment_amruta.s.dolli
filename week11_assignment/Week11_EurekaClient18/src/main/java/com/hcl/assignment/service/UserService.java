package com.hcl.assignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.assignment.bean.User;
import com.hcl.assignment.dao.UserDao;

@Service
public class UserService {
	@Autowired
	UserDao userDao;

	public String userLogin(User user) {
		if (userDao.existsById(user.getId())) {
			User u = userDao.getById(user.getId());
			if (u.getName().equals(u.getName())) {
				return "you logged in succesfully";
			} else {
				return "please enter valid details";
			}
		} else {
			return "please enter valid details";
		}
	}

	public String userLogout(User user) {
		if (userDao.existsById(user.getId())) {
			User u = userDao.getById(user.getId());
			if (u.getName().equals(u.getName())) {
				return "Logged out succesfully";
			} else {
				return "please enter crrct details";
			}
		} else {
			return "please enter crrct details";
		}
	}

	public List<User> getAllusers() {
		return userDao.findAll();
	}

	public String register(User user) {
		if (userDao.existsById(user.getId())) {
			return "your details already present";
		} else {
			userDao.save(user);
			return "user registered succesfully";
		}
	}

}
