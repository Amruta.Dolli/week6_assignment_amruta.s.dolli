package com.hcl.assignment.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.assignment.bean.Book;

@Repository
public interface BookDao extends JpaRepository<Book, Integer> {

}
