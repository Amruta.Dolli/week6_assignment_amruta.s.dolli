package com.hcl.assignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.assignment.bean.Admin;
import com.hcl.assignment.dao.AdminDao;

@Service
public class AdminService {
	@Autowired
	AdminDao adminDao;

	public String adminLogin(Admin admin) {
		if (adminDao.existsById(admin.getPassword())) {
			Admin a = adminDao.getById(admin.getPassword());
			if (a.getUsername().equals(admin.getUsername())) {
				return "you logged in succesfully";
			} else {
				return "please enter valid details";
			}
		} else {
			return "please enter valid details";
		}

	}

	public String adminLogout(Admin admin) {
		if (adminDao.existsById(admin.getPassword())) {
			Admin a = adminDao.getById(admin.getPassword());
			if (a.getUsername().equals(admin.getUsername())) {
				return "Logged out succesfully";
			} else {
				return "please enter crrct details";
			}
		} else {
			return "please enter crrct details";
		}
	}

	public List<Admin> getAllusers() {
		return adminDao.findAll();
	}

	public String register(Admin admin) {
		if (adminDao.existsById(admin.getPassword())) {
			return "your details already present";
		} else {
			adminDao.save(admin);
			return "admin registered succesfully";
		}
	}

}
