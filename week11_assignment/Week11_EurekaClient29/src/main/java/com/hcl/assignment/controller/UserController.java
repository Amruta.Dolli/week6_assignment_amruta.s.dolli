package com.hcl.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.assignment.bean.User;
import com.hcl.assignment.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	UserService userService;

	@GetMapping(value = "getAllUser", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<User> getAllUsers() {
		return userService.getAllUser();
	}

	@PostMapping(value = "storeUser", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeUser(@RequestBody User user) {
		return userService.storeUser(user);
	}

	@RequestMapping(value = "deleteUser/{id}", method = RequestMethod.DELETE)
	public String deleteBook(@PathVariable("id") int id) {
		return userService.deleteUser(id);
	}

	@RequestMapping(value = "updateUser", method = RequestMethod.PUT)
	public String updateBook(@RequestBody User user) {
		return userService.updateUser(user);

	}

}
