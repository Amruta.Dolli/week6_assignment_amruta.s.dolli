package com.hcl.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.assignment.bean.Admin;
import com.hcl.assignment.service.AdminService;

@RestController
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	AdminService adminService;

	@PostMapping(value = "loginAdmin", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String login(@RequestBody Admin admin) {
		return adminService.adminLogin(admin);
	}

	@PostMapping(value = "logoutAdmin", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String logout(@RequestBody Admin admin) {
		return adminService.adminLogout(admin);
	}

	@GetMapping(value = "getAllusers", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Admin> getAllUsers() {
		return adminService.getAllusers();
	}

	@PostMapping(value = "registerAdmin", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String register(@RequestBody Admin admin) {
		return adminService.register(admin);
	}
}
