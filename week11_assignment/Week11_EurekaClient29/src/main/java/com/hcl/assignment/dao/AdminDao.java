package com.hcl.assignment.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.assignment.bean.Admin;

@Repository
public interface AdminDao extends JpaRepository<Admin, Integer> {

}
