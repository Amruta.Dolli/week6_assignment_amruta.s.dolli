package com.hcl.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.assignment.bean.Book;
import com.hcl.assignment.service.BookService;

@RestController
@RequestMapping("/book")
public class BookController {
	@Autowired
	BookService bookService;

	@GetMapping(value = "getAllBook", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Book> getAllBooks() {
		return bookService.getAllbooks();
	}

	@PostMapping(value = "storeBook", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeBook(@RequestBody Book book) {
		return bookService.storeBook(book);
	}

	@RequestMapping(value = "deleteBook/{id}", method = RequestMethod.DELETE)
	public String deleteBook(@PathVariable("id") int id) {
		return bookService.deleteBook(id);
	}

	@RequestMapping(value = "updateBook", method = RequestMethod.PUT)
	public String updateBook(@RequestBody Book book) {
		return bookService.updateBook(book);

	}

}
