package com.hcl.assignment.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.hcl.assignment.bean.Admin;
import com.hcl.assignment.bean.User;
import com.hcl.assignment.dao.AdminDao;
import com.hcl.assignment.service.AdminService;

@SpringBootTest
class AdminServiceTest {
	
	@InjectMocks
	AdminService adminService;
	
	@Mock
	AdminDao adminDao;
	
	
	@Test
	void testGetAllusers() {
		//fail("Not yet implemented");
		List<Admin> list = new ArrayList<Admin>();
        Admin a1 = new Admin();
		Admin a2 = new Admin();
		Admin a3 = new Admin();
		list.add(a1);

		list.add(a2);
		list.add(a3);
		when(adminDao.findAll()).thenReturn(list);
		List<Admin> adminList = adminService.getAllusers();
		assertEquals(3, adminList.size());
		verify(adminDao, times(1)).findAll();
	}

	@Test
	void testRegister() {
		//fail("Not yet implemented");
		Admin admin = new Admin();
		adminService.register(admin);
		verify(adminDao, times(1)).save(admin);
	}


//	@Test
//	void testAdminLogin() {
//		//fail("Not yet implemented");
//		Admin admin = new Admin();
//		adminService.adminLogin(admin);
//		verify(adminDao, times(1)).save(admin);
//	}

//	@Test
//	void testAdminLogout() {
//		fail("Not yet implemented");
//	}

	
}
