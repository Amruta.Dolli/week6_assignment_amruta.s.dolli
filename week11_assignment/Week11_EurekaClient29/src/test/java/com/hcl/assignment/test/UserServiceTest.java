package com.hcl.assignment.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.hcl.assignment.bean.Book;
import com.hcl.assignment.bean.User;
import com.hcl.assignment.dao.UserDao;
import com.hcl.assignment.service.UserService;

@SpringBootTest
class UserServiceTest {

	@InjectMocks
	UserService userService;
	
	@Mock
	UserDao userDao;
	@Test
	void testGetAllUser() {
		//fail("Not yet implemented");
		List<User> list = new ArrayList<User>();
		User u1 = new User();
		User u2 = new User();
		User u3 = new User();
		list.add(u1);

		list.add(u2);
		list.add(u3);
		when(userDao.findAll()).thenReturn(list);
		List<User> userList = userService.getAllUser();
		assertEquals(3, userList.size());
		verify(userDao, times(1)).findAll();
	}

	@Test
	void testStoreUser() {
		//fail("Not yet implemented");
		User user = new User();
		userService.storeUser(user);
		verify(userDao, times(1)).save(user);
	}

	@Test
	void testDeleteUser() {
		//fail("Not yet implemented");
	User user = new User(1,"amruta","6899");
		userDao.save(user);
		userDao.deleteById(user.getId());
		Optional optional =userDao.findById(user.getId());
		assertEquals(Optional.empty(), optional);

	}

//	@Test
//	void testUpdateUser() {
//		fail("Not yet implemented");
//	}

}
