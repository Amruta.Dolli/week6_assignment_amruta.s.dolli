create database mini_hcl;
use mini_hcl;
create table admin(email varchar(20) primary key,password varchar(20));

insert into admin values("amruta@gmail.com","amruta123");

create table users(email varchar(20) primary key,password varchar(20));

create table items(id int primary key,iname varchar(50),type varchar(50),price float);
insert into items values(1,"dosa","tiffin",45);
 insert into items values(2,"idli","tiffin",35);
insert into items values(3,"pav baji","tiffin",50);
insert into items values(4,"gobi","tiffin",80);
insert into items values(5,"pulav","tiffin",100);

create table userorder(sno int primary key auto_increment,id int ,iname varchar(50),type varchar(50),price float,name varchar(20));
 