package com.controller;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.bean.Menu;
import com.bean.Users;
import com.service.MenuService;

@Controller
public class MenuController {
	@Autowired
	MenuService itemsService;

	List<Menu> listOfItems;
	
	@GetMapping(value = "ListOfItems")
	public String getAllItemsForUser(HttpSession hs) {
		listOfItems = itemsService.getAllItems();
		hs.setAttribute("items", listOfItems);
		return "adminMenu";
	}


	@PostMapping(value = "storeMenuitem")
	public String storeItem(HttpServletRequest req, HttpSession hs) {
		int itemId = Integer.parseInt(req.getParameter("itemId"));
		String itemName = req.getParameter("itemName");
		String itemType = req.getParameter("itemType");
		float itemPrice = Float.parseFloat(req.getParameter("itemPrice"));
		Menu item = new Menu();
		item.setItemId(itemId);
		item.setItemName(itemName);
		item.setItemType(itemType);
		item.setItemPrice(itemPrice);
		String res = itemsService.addItem(item);
		hs.setAttribute("res2", res);
		return "messagepage";
	}

	@GetMapping(value = "menuDetails")
	public String storeMenu() {
		return "menuDetails";
	}

	@GetMapping(value = "menuList")
	public String getAllItems(HttpSession hs) {
		listOfItems = itemsService.getAllItems();
		hs.setAttribute("items", listOfItems);
		return "menu";
	}

	
	@GetMapping(value = "deleteItem")
	public String deleteItem(HttpServletRequest req, HttpSession hs) {
		int itemId = Integer.parseInt(req.getParameter("itemId"));
		String result = itemsService.deleteItem(itemId);
		hs.setAttribute("result", result);
		listOfItems = itemsService.getAllItems();
		hs.setAttribute("items", listOfItems);
		return "adminMenu";
	}

	@GetMapping(value = "UpdateItem")
	public String updateItem(HttpServletRequest req, HttpSession hs) {
		int itemId = Integer.parseInt(req.getParameter("itemId"));
		hs.setAttribute("itemId", itemId);
		return "updateItem";
	}

	@PostMapping(value = "updateMenuitem")
	public String updateItemInfo(HttpServletRequest req, HttpSession hs) {
		int itemId = Integer.parseInt(req.getParameter("itemId"));
		String itemName = req.getParameter("itemName");
		String itemType = req.getParameter("itemType");
		float itemPrice = Float.parseFloat(req.getParameter("itemPrice"));

		Menu item = new Menu();
		item.setItemId(itemId);
		item.setItemName(itemName);
		item.setItemType(itemType);
		item.setItemPrice(itemPrice);

		String result = itemsService.updatePrice(item);
		hs.setAttribute("result", result);
		listOfItems = itemsService.getAllItems();
		hs.setAttribute("items", listOfItems);
		return "adminMenu";
	}

}
