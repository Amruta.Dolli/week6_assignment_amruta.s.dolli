package com.controller;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.bean.Menu;
import com.bean.Order;
import com.dao.OrderDao;
import com.service.OrderService;


@Controller
public class OrderController {
	
	@Autowired
	OrderService orderService;
	
	List<Menu> listOfItems;
	
	@SuppressWarnings("unchecked")
	@PostMapping(value="order")
	public String getorder(HttpServletRequest req,HttpSession hs) {
		int itemId=Integer.parseInt(req.getParameter("itemId"));
		int noOfPlates=Integer.parseInt(req.getParameter("noOfPlates"));
		String name=(String) hs.getAttribute("name");
		String orderResult;
		listOfItems=(List<Menu>) hs.getAttribute("items");
		Iterator<Menu>li=listOfItems.iterator();
		while(li.hasNext()) {
			Menu itm=li.next();
					
			if(itemId==itm.getItemId()) {
				Order order=new Order();
				order.setItemId(itm.getItemId());
				order.setItemName(itm.getItemName());
				order.setItemType(itm.getItemType());
				order.setItemPrice(itm.getItemPrice()*noOfPlates);			
				order.setName(name);
				orderResult=orderService.addtoOrder(order);
			}
	                
		}	            
					return "menu";
	}
	
	@GetMapping(value="orderedList")
	public String  getOrderList(HttpSession hs){
		String name=(String) hs.getAttribute("name");
		List<Order> res= orderService.AllIOrderedItem(name);
		hs.setAttribute("lo", res);
		return "orderedList";
	}
	@GetMapping(value="totalBill")
	public String getTotal(HttpSession hs) {
		String name=(String) hs.getAttribute("name");
		float totalprice=orderService.getTotalBill(name);
		hs.setAttribute("totalbill", totalprice);
		return "totalbill";
		
	}
	
	@GetMapping(value="todaybill")
	public String getTodayOrder(HttpSession hs) {
	List<Order> uo=	orderService.getAllBillsGeneratedToday();
	hs.setAttribute("uo", uo);
		return "todayorders";
	}
	
	@GetMapping(value="todayTotalbill")
	public String getTodayBill(HttpSession hs) {
		float todayEarnings=orderService.getTodaybill();
		hs.setAttribute("totsum",todayEarnings);
		return "todayTotalbill";
	}

}
