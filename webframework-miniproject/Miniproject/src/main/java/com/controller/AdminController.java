package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bean.Admin;
import com.service.AdminService;

@Controller
public class AdminController {
	@Autowired
	AdminService adminService;
	
	@GetMapping(value="index")
	public String openInsex() {
		return "index";
	}
	
	@GetMapping(value="adminLogin")
	public String adminLogin(HttpServletRequest req,HttpSession hs) {
		String email=req.getParameter("email");
		String password=req.getParameter("pass");
		Admin adm=new Admin();
		adm.setEmail(email);
		adm.setPassword(password);
		String res=adminService.adminLogin(adm);
		if(res.equalsIgnoreCase("sucess")) {
			return "adminPage";
		}else {
			hs.setAttribute("ad1","please enter valid details");
			return "admin";
		}
	}
	@GetMapping(value="admin")
	public String adminPage() {
		return "admin";
	}
	@GetMapping(value = "user")
	public String userLogin() {
		return "userlogin";
	}
	@GetMapping(value = "registration")
	public String userRegistration() {	
		return "userRegistration";
	}
	

	@GetMapping(value="logout")
	public String adminLogout() {
		return"logout";
	}


}
