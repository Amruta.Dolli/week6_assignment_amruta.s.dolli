package com.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bean.Menu;
import com.bean.Users;
import com.service.UsersService;

@Controller
public class UsersController {
	@Autowired
	UsersService usersService;
	
	@GetMapping(value="userList")
	public String getAllUserInfo(HttpSession hs) {
		List<Users>listOfUsers=usersService.getAllUsersInfo();
		hs.setAttribute("user", listOfUsers);
		return "userList";
	}
	
	@PostMapping(value = "addUser")
	public String storeUserInfo(HttpServletRequest req ,HttpSession hs) {
		String email=req.getParameter("email");
		String password=req.getParameter("pass");
		Users user=new Users();
		user.setEmail(email);
		user.setPassword(password);
		String failure="your details already present";
		String sucess="Registered sucessfully";
		String res=usersService.userRegister(user);
		if(res.equalsIgnoreCase(sucess)) {
			req.setAttribute("msg1", sucess);
			return "userlogin";			
		}else {
			req.setAttribute("msg2",failure);
			return "userlogin";
		}				
	}
	@GetMapping(value = "checkUser")
	public String checkUserDetails(HttpServletRequest req,HttpSession hs) {
		String email=req.getParameter("email");
		String password=req.getParameter("pass");
		String name=email.substring(0,email.indexOf('@'));
		Users user=new Users();
		user.setEmail(email);
		user.setPassword(password);
		String res=usersService.userLogin(user);
		if(res.equalsIgnoreCase("sucess")) {
			hs.setAttribute("name", name);
			return "welcome";
		}else {
			req.setAttribute("us1","please enter valid login details");
			return "userlogin";
		}
	}
	
	
	
	@GetMapping(value="deleteUser")
	public String deleteUser(HttpServletRequest req,HttpSession hs) {
		String email=req.getParameter("email");
		String res=usersService.deleteUser(email);
		req.setAttribute("res", res);
		List<Users>listOfUsers=usersService.getAllUsersInfo();
		hs.setAttribute("user", listOfUsers);
		return "userList";				
	}
	
	@GetMapping(value="UpdateUser")
	public String updateUser(HttpServletRequest req,HttpSession hs) {
		String email=req.getParameter("email");
		hs.setAttribute("email", email);
		return "userUpdate";
	}
	
	@PostMapping(value="updateUserDetails")
	public String updateUserInfo(HttpServletRequest req,HttpSession hs) {
		String email=req.getParameter("email");
		String password=req.getParameter("password");
		Users user=new Users();
		user.setEmail(email);
		user.setPassword(password);
		String res=usersService.updateUserInfo(user);
		req.setAttribute("res1", res);
		List<Users>listOfUsers=usersService.getAllUsersInfo();
		hs.setAttribute("user", listOfUsers);
		return "userList";			
	}
	
	
	@GetMapping(value="logoutUser")
	public String userLogout() {
		return "logout";
	}
	
	

}
