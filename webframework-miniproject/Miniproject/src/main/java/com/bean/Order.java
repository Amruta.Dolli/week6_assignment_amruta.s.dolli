package com.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="userorder")
public class Order {
	@Id
	@Column(name="sno")
	private int sno;
	@Column(name="id")
	private int ItemId;
	@Column(name="iname")
	private String ItemName;
	@Column(name="type")
	private String ItemType;
	@Column(name="price")
	private float itemPrice;
	@Column(name="name")
	private String name;
	
	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public Order(int sno, int itemId, String itemName, String itemType, float itemPrice, String name) {
		super();
		this.sno = sno;
		ItemId = itemId;
		ItemName = itemName;
		ItemType = itemType;
		this.itemPrice = itemPrice;
		this.name = name;
	}



	public int getSno() {
		return sno;
	}
	public void setSno(int sno) {
		this.sno = sno;
	}
	public int getItemId() {
		return ItemId;
	}
	public void setItemId(int itemId) {
		ItemId = itemId;
	}
	public String getItemName() {
		return ItemName;
	}
	public void setItemName(String itemName) {
		ItemName = itemName;
	}
	public String getItemType() {
		return ItemType;
	}
	public void setItemType(String itemType) {
		ItemType = itemType;
	}
	public float getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(float itemPrice) {
		this.itemPrice = itemPrice;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Order [sno=" + sno + ", ItemId=" + ItemId + ", ItemName=" + ItemName + ", ItemType=" + ItemType
				+ ", itemPrice=" + itemPrice + ", name=" + name + "]";
	}

	
	

}
