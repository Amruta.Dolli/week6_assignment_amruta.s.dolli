package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Admin;
import com.dao.AdminDao;

@Service
public class AdminService {
	@Autowired 
	AdminDao adminDao;
	
	public String adminLogin(Admin adm) {
		
		if(adminDao.existsById(adm.getEmail())) {
			Admin a=adminDao.getById(adm.getEmail());
			if(a.getPassword().equals(adm.getPassword())) {
				return "sucess";
			}else {
				return "failure";
			}
		}else {
			return "failed";
		}
		
	}

}
