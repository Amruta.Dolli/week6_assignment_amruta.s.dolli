package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Users;
import com.dao.UsersDao;

@Service
public class UsersService {
	@Autowired
	UsersDao usersDao;
	public String userRegister(Users user) {
		if(usersDao.existsById(user.getEmail())) {
			return "your details already present";
		}else {
			usersDao.save(user);
			return "Registered sucessfully";
		}
	}
	public String userLogin(Users user) {
		if(usersDao.existsById(user.getEmail())) {
			Users u=usersDao.getById(user.getEmail());
			if(u.getPassword().equals(user.getPassword())) {
				return "sucess";
			}else {
				return "failure";
			}
		}else {
			return "you need to register";
		}
	}
	
	public List<Users> getAllUsersInfo(){
		return usersDao.findAll();
	}
	
	public String deleteUser(String email) {
		usersDao.deleteById(email);
		return "user deleted sucessfully";
		
	}
	
	public String updateUserInfo(Users user) {
		Users u=usersDao.getById(user.getEmail());
		u.setPassword(user.getPassword());
		usersDao.saveAndFlush(u);
		return "user updated sucessfully";
	}
	

}
