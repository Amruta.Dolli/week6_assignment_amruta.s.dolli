package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Order;
import com.dao.OrderDao;

@Service
public class OrderService {

	@Autowired
	OrderDao orderDao;

	public List<Order> AllIOrderedItem(String name) {
		return orderDao.getItemsOrderedByName(name);
	}

	public float getTotalBill(String name) {
		return orderDao.getTotalBillByName(name);
	}

	public List<Order> getAllBillsGeneratedToday() {
		return orderDao.findAll();
	}

	public float getTodaybill() {
		return orderDao.getTodayTotalEarnings();
	}

	public String addtoOrder(Order order) {
		int sno = (int) orderDao.count();
		order.setSno(sno + 1);
		orderDao.save(order);
		return "orderStored sucessfully";
	}

}
