package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Menu;
import com.dao.MenuDao;

@Service
public class MenuService {
	@Autowired
	MenuDao menuDao;
	
	public String   addItem(Menu menu) {
		if(menuDao.existsById(menu.getItemId())) {
			return "itemId already exits";
		}else {
			menuDao.save(menu);
			return "items stored sucessfully";
		}
	}
	
	public List<Menu> getAllItems(){
		return menuDao.findAll();
	}
	
	public String deleteItem(int itemId) {
		if (!menuDao.existsById(itemId)) {
			return "item details not found";
		} else {
			menuDao.deleteById(itemId);
			return "item deleted succesfully";
		}
	}
	
	public String updatePrice(Menu menu) {
		Menu item=menuDao.getById(menu.getItemId());
		item.setItemName(menu.getItemName());
		item.setItemType(menu.getItemType());
		item.setItemPrice(menu.getItemPrice());
		menuDao.saveAndFlush(item);
		return "Item Price Updated Sucessfully";
	}

}
