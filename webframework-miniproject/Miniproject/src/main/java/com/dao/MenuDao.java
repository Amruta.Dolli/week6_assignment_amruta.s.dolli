package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bean.Menu;

@Repository
public interface MenuDao extends JpaRepository<Menu, Integer> {

}
