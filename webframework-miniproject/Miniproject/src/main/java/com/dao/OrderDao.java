package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bean.Order;

@Repository

public interface OrderDao extends JpaRepository<Order, Integer>{
	@Query("select o from Order o where o.name=:name")
	public List<Order> getItemsOrderedByName(@Param("name") String name);
	
	@Query("select sum(o.itemPrice) from Order o where o.name=:name")
	public float getTotalBillByName(@Param("name")String name);
	
	@Query("select sum(o.itemPrice) from Order o")
	public float getTodayTotalEarnings();

}
