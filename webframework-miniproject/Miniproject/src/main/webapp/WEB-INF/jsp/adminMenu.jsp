<%@page import="java.util.*"%>
<%@page import="com.bean.Menu"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div>
		<table border="2">
			<tr>
				<th>ITEMId</th>
				<th>ITEMNAME</th>
				<th>ITEMTYPE</th>
				<th>ITEMPRICE</th>
				<th>DELETE</th>
				<th>UPDATE</th>

			</tr>
			<%
				Object res = session.getAttribute("r1");
			if (res != null) {
				out.println(res);
			}
			Object obj = session.getAttribute("items");
			List<Menu> listOfItems = (List<Menu>) obj;
			Iterator<Menu> ii = listOfItems.iterator();
			while (ii.hasNext()) {
				Menu item = ii.next();
			%>
			<tr>
				<td><%=item.getItemId()%></td>
				<td><%=item.getItemName()%></td>
				<td><%=item.getItemType()%></td>
				<td><%=item.getItemPrice()%></td>
				<td><a href="deleteItem?itemId=<%=item.getItemId()%>">Delete</a>
				</td>
				<td><a href="UpdateItem?itemId=<%=item.getItemId()%>">Update</a>
				</td>
			</tr>
			<%
				}
			%>
		</table>
	</div>
	<br>
	<a href="menuDetails">add item</a>
	<br>

	<a href="logout">Logout</a>
</body>
</html>