<%@page import="com.bean.Users"%>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body bgcolor="lightblue">
<h3 align="center">Users Have Loggedin successfully</h3>
<div align="center">
<h2>List of users</h2>
<table border="2">
	<tr>
			<th>EmailId</th>
			<th>Password</th>
			<th>Delete</th>
			<th>Update</th>
			
	</tr>
<%  
    Object res=request.getAttribute("res");
    if(res!=null){
    out.println(res);
    }
    Object res1=request.getAttribute("r2");
    if(res1!=null){
    	out.println(res1);
    }
    Object obj = session.getAttribute("user");
	List<Users> listOfItems = (List<Users>)obj;
	Iterator<Users> ii = listOfItems.iterator();
	while(ii.hasNext()){
		Users user = ii.next();
		%>
		<tr>
			<td><%=user.getEmail() %></td>
			<td><%=user.getPassword()%></td>
	        <td><a href="deleteUser?email=<%=user.getEmail()%>">Delete</a> </td>
			<td><a href="UpdateUser?email=<%=user.getEmail()%>">Update</a> </td>
		</tr>
		<% 
	}
%>
</table>
</div>
<a href="registration">store user info</a><br>
<a href="logout" align="right">Logout</a>
</body>
</html>