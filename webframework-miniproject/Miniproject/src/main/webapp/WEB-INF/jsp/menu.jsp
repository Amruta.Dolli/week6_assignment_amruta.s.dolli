<%@page import="java.util.*"%>
<%@page import="com.bean.Menu"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h2>WELCOME TO RESTAURENT</h2>
	<div align="center">
		<table border="1" bgcolor="lightyellow">
			<tr bgcolor="lightyellow">
				<th>ITEMId</th>
				<th>ITEMNAME</th>
				<th>ITEMTYPE</th>
				<th>ITEMPRICE</th>
				<th>ORDER</th>

			</tr>
			<%
				Object name = session.getAttribute("name");
			if (name != null) {
				out.println("These Are The Items Available In Our Store Ms/Mr  " + name);
			}
			Object obj = session.getAttribute("items");
			List<Menu> listOfItems = (List<Menu>) obj;
			Iterator<Menu> ii = listOfItems.iterator();
			while (ii.hasNext()) {
				Menu item = ii.next();
			%>
			<tr>
				<td><%=item.getItemId()%></td>
				<td><%=item.getItemName()%></td>
				<td><%=item.getItemType()%></td>
				<td><%=item.getItemPrice()%></td>

				<td><form action="order" method="post">
						<input type="hidden" name="itemId" value=<%=item.getItemId()%>>
						<input type="number" min="1" max="10" value="1" name="noOfPlates">
						<input type="submit" value="order" name="order"></input>
					</form></td>
			</tr>
			<%
				}
			%>

		</table>
	</div>
	<a href="orderedList">ordered list</a>
	<br>
	<a href="logoutUser">Logout</a>
	<br>

</body>
</html>