<%@page import="com.bean.Order"%>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body bgcolor="lightblue">
<div>
<table border="2">
	<tr>
	        <th>SNO</th>
			<th>ITEMId</th>
			<th>ITEMNAME</th>
			<th>ITEMTYPE</th>
			<th>ITEMPRICE</th>
			<th>User</th>
			
			
	</tr>
<%
	Object obj = session.getAttribute("uo");
	List<Order> listOfItems = (List<Order>)obj;
	Iterator<Order> ii = listOfItems.iterator();
	while(ii.hasNext()){
		Order uo  = ii.next();
%>
		<tr>
		    <td><%=uo.getSno() %></td>
			<td><%=uo.getItemId() %></td>
			<td><%=uo.getItemName()%></td>
			<td><%=uo.getItemType() %></td>
			<td><%=uo.getItemPrice() %></td>
			<td><%=uo.getName() %></td>
		</tr>
		<% 
	}
%>

</table>
</div>
<a href="todayTotalbill">Today total bill</a>
</body>
</html>