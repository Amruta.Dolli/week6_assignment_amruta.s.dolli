package com.greatLearning.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.greatLearning.bean.User;
import com.greatLearning.dao.UserDao;
import com.greatLearning.resource.DbResource;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
//	public RegisterServlet() {
//		super();
//
//	}
//
//	/**
//	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
//	 *      response)
//	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//
//		response.getWriter().append("Served at: ").append(request.getContextPath());
//	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// doGet(request, response);
		try (PrintWriter out = response.getWriter()) {
			out.println("<!DOCTYPE html>");
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Servlet  RegisterServlet</title>");
			out.println("</head>");
			out.println("<body>");

			String name = request.getParameter("name");
			String email = request.getParameter("email");
			String password = request.getParameter("password");

			User userModel = new User(name, email, password);

			UserDao regUser = new UserDao(DbResource.getConnection());
			if (regUser.saveUser(userModel)) {
				out.println("<p style='color:red'> user info saved successfully.!</p>");
				out.println("<a href='index.jsp'> go to login...</a>");
			} else {
				String errorMessage = "User Available";
				HttpSession regSession = request.getSession();
				regSession.setAttribute("RegError", errorMessage);
				response.sendRedirect("register.jsp");
			}

			out.println("</body>");
			out.println("</html>");
		}
	}
}
