package com.greatLearning.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.greatLearning.bean.User;
import com.greatLearning.dao.UserDao;
import com.greatLearning.resource.DbResource;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
//	public LoginServlet() {
//		super();
//
//	}
//
//	/**
//	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
//	 *      response)
//	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//
//		// response.getWriter().append("Served at: ").append(request.getContextPath());
//	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// doGet(request, response);
		response.setContentType("text/html/;charset=UTF-8");
		try (PrintWriter out = response.getWriter()) {
			out.println("<!DOCTYPE html>");
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Servlet  RegisterServlet</title>");
			out.println("</head>");
			out.println("<body>");

			String logemail = request.getParameter("email");
			String logpass = request.getParameter("password");

			UserDao db = new UserDao(DbResource.getConnection());
			User user = db.login(logemail, logpass);

			if (user != null) {
				HttpSession session = request.getSession();
				session.setAttribute("logUser", user);

				response.sendRedirect("welcome.jsp");
			} else {
				out.println("user not found");
			}
			out.println("</body>");
			out.println("</html>");
		}
	}

}
