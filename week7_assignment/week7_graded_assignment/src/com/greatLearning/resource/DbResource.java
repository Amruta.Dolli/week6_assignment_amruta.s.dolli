package com.greatLearning.resource;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbResource {
	private static Connection con;

	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/week7_books", "root", "1234");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;
	}
}
