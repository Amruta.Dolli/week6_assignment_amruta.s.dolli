package com.greatLearning.bean;

public class Like {
	private int likedBookId;
	private String userName;
	private int bookId;
	private String title;
	private int totalPages;
	private float rating;
	private int isbn;

	public Like(int likedBookId, String userName, int bookId, String title, int totalPages, float rating, int isbn) {
		super();
		this.likedBookId = likedBookId;
		this.userName = userName;
		this.bookId = bookId;
		this.title = title;
		this.totalPages = totalPages;
		this.rating = rating;
		this.isbn = isbn;
	}

	public Like() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getLikedBookId() {
		return likedBookId;
	}

	public void setLikedBookId(int likedBookId) {
		this.likedBookId = likedBookId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public int getIsbn() {
		return isbn;
	}

	public void setIsbn(int isbn) {
		this.isbn = isbn;
	}

	@Override
	public String toString() {
		return "Like [likedBookIid=" + likedBookId + ", userName=" + userName + ", bookId=" + bookId + ", title="
				+ title + ", totalPages=" + totalPages + ", rating=" + rating + ", isbn=" + isbn + "]";
	}

}
