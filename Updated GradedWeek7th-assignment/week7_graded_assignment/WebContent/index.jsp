<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>

<%
	String id = request.getParameter("userid");
String driver = "com.mysql.cj.jdbc.Driver";
String connectionUrl = "jdbc:mysql://localhost:3306/";
String database = "week7_books";
String userid = "root";
String password = "1234";

try {

	Class.forName(driver);

} catch (ClassNotFoundException e) {

	e.printStackTrace();

}

Connection connection = null;
Statement statement = null;
ResultSet resultSet = null;
%>

<!DOCTYPE html>

<html>

<head>

<meta charset="ISO-8859-1">

<title>Home Page</title>

<link href="https://fonts.googleapis.com/css?family=ZCOOL+XiaoWei"
	rel="stylesheet">
<link href="css/style.css" rel="stylesheet" type="text/css" />

</head>

<body style="text-align: center;">
	<div class="container">

		<h1>WelCome To Bookshop</h1>
		<br> <a href="login.jsp">Login...</a> <br> 
		<a href="registration.jsp">New Registration</a> <br>

		<div class="container">
			<div class="container">
				<div class="container">
					<table border="1">
						<tr>
							<td>bookId</td>
							<td>title</td>
							<td>totalPages</td>
							<td>rating</td>
							<td>isbn</td>
						</tr>

						<%
							try {

							connection = DriverManager.getConnection(connectionUrl + database, userid, password);
							statement = connection.createStatement();
							String sql = "select * from books";
							resultSet = statement.executeQuery(sql);

							while (resultSet.next()) {
						%>

						<tr>
							<td><%=resultSet.getString("bookId")%></td>
							<td><%=resultSet.getString("title")%></td>
							<td><%=resultSet.getString("totalPages")%></td>
							<td><%=resultSet.getString("rating")%></td>
							<td><%=resultSet.getString("isbn")%></td>
						</tr>

						<%
							}

						connection.close();

						} catch (Exception e) {

							e.printStackTrace();

						}
						%>

					</table>
				</div>
			</div>
		</div>
	</div>
</body>
</html>