package com.hcl.assignment.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Admin {
	@Id
	private int password;
	private String username;

	public Admin() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Admin(int password, String username) {
		super();
		this.password = password;
		this.username = username;
	}

	public int getPassword() {
		return password;
	}

	public void setPassword(int password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return "Admin [password=" + password + ", username=" + username + "]";
	}

}
