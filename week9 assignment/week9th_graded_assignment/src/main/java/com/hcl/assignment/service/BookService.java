package com.hcl.assignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.assignment.bean.Book;
import com.hcl.assignment.dao.BookDao;

@Service
public class BookService {

	@Autowired
	BookDao bookDao;

	public List<Book> getAllbooks() {
		return bookDao.findAll();
	}

	public String storeBook(Book book) {
		if(bookDao.existsById(book.getId())) {
			return "id must be unique";
		}else {
		bookDao.save(book);
		return "book stored succesfully";
		}

	}

	public String deleteBook(int id) {

		bookDao.deleteById(id);

		return "Book deleted succesfully";

	}

	public String updateBook(Book book) {
		if (!bookDao.existsById(book.getId())) {
			return "book is not present";
		} else {
			bookDao.save(book);
			return "book updated succesfully";
		}
	}
}
