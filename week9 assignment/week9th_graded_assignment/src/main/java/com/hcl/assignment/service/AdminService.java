package com.hcl.assignment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.assignment.bean.Admin;
import com.hcl.assignment.dao.AdminDao;

@Service
public class AdminService {

	@Autowired
	AdminDao adminDao;

	public Admin login(Admin admin) {
		Admin newUser = new Admin(1234, "amruta");

		return adminDao.save(newUser);
	}

	public Admin logPot(Admin admin) {
		Admin newUser = new Admin(1234, "amruta");
		return adminDao.save(newUser);
	}
}
