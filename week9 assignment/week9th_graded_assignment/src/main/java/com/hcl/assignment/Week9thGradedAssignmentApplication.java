package com.hcl.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "com.hcl.assignment")
@EnableJpaRepositories(basePackages = "com.hcl.assignment.dao")
@EntityScan(basePackages = "com.hcl.assignment.bean")
public class Week9thGradedAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(Week9thGradedAssignmentApplication.class, args);
	}

}
