package com.hcl.assignment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hcl.assignment.bean.Admin;
import com.hcl.assignment.service.AdminService;

@Controller
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	AdminService adminService;

	@RequestMapping(value = "/loginAdmin", method = RequestMethod.POST)
	public String login(@RequestBody Admin admin) {

		admin.getPassword();
		admin.getUsername();
		return "login succesfully";

	}

	@RequestMapping(value = "/logOutAdmin", method = RequestMethod.POST)
	public String logOut(@RequestBody Admin admin) {

		admin.getPassword();
		admin.getUsername();
		return "logout succesfully";
	}
}
