package com.hcl.assignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.assignment.bean.Book;
import com.hcl.assignment.bean.User;
import com.hcl.assignment.dao.UserDao;

@Service
public class UserService {
	@Autowired
	UserDao userDao;

	public List<User> getAllUser() {
		return userDao.findAll();
	}

	public String storeUser(User user) {
		if (userDao.existsById(user.getId())) {
			return "id must be unique";
		} else {
			userDao.save(user);
			return "user stored succesfully";
		}
	}

	public String deleteUser(int id) {

		userDao.deleteById(id);
		return "user deleted succesfully";

	}

	public String updateUser(User user) {
		if (!userDao.existsById(user.getId())) {
			return "user is not present";
		} else {
			userDao.save(user);
			return "user updated succesfully";
		}
	}

}
