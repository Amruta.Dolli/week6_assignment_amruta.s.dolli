<%@page import="java.sql.DriverManager"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>

<%
	String id = request.getParameter("userid");

String driver = "com.mysql.cj.jdbc.Driver";
String connectionUrl = "jdbc:mysql://localhost:3306/";
String database = "week8_books";
String userid = "root";
String password = "1234";

try {

	Class.forName(driver);

} catch (ClassNotFoundException e) {

	e.printStackTrace();

}

Connection connection = null;
Statement statement = null;
ResultSet resultSet = null;
%>

<!DOCTYPE html>

<html>

<head>

<meta charset="ISO-8859-1">

<title>Welcome Page</title>
</head>

<body style="text-align: center;">
	<div>

		<h1>WelCome To Bookess</h1>
		<jsp:include page="name.jsp"></jsp:include>
		<br>
		<div>

			<table border="1">
				<tr>
					<td>bookId</td>
					<td>title</td>
					<td>totalPages</td>
					<td>rating</td>
					<td>isbn</td>
					<td>read_later_section</td>
					<td>like_section</td>
				</tr>

				<%
					try {

					connection = DriverManager.getConnection(connectionUrl + database, userid, password);
					statement = connection.createStatement();
					String sql = "select * from books";
					resultSet = statement.executeQuery(sql);

					while (resultSet.next()) {
				%>

				<tr>
					<td><%=resultSet.getString("bookId")%></td>
					<td><%=resultSet.getString("title")%></td>
					<td><%=resultSet.getString("totalPages")%></td>
					<td><%=resultSet.getString("rating")%></td>
					<td><%=resultSet.getString("isbn")%></td>
					<td><a href="readLater.jsp"><button type="button">ReadLater</button></a></td>
				<td><a href="like.jsp"><button type="button">Like</button></a></td>
			

					<%
						}

					connection.close();

					} catch (Exception e) {

						e.printStackTrace();

					}
					%>
				
			</table>
		</div>
	</div>
	<a href="readSuccess.jsp">read later books</a>
	<br>
	<a href="likedbook.jsp">liked books</a>
</body>
</html>
