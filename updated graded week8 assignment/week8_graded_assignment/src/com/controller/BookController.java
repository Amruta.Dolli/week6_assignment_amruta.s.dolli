
package com.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bean.Books;
import com.dao.BooksDao;
import com.service.BookService;

@Controller
public class BookController {
	@Autowired
	BookService bookService;

	@RequestMapping(value = "displayBook", method = RequestMethod.GET)
	public ModelAndView getAllBook(HttpServletRequest req) {
		ModelAndView mav = new ModelAndView();
		int bookId = Integer.parseInt(req.getParameter("bookId"));
		System.out.println("book id is " + bookId);
		List<Books> listOfBooks = bookService.getBooks();
		req.setAttribute("Books", listOfBooks);
		mav.setViewName("viewBook.jsp");
		return mav;
	}

	@RequestMapping(value = "likeBook", method = RequestMethod.GET)
	public ModelAndView likeBook(HttpServletRequest req) {
		ModelAndView mav = new ModelAndView();
		int bookId = Integer.parseInt(req.getParameter("bookId"));
		System.out.println("book id is " + bookId);
		List<Books> listOfBooks = bookService.getAllBooksFromLikeSection();
		req.setAttribute("Books", listOfBooks);
		mav.setViewName("like.jsp");
		return mav;
	}

	@RequestMapping(value = "readLater", method = RequestMethod.GET)
	public ModelAndView readLaterBook(HttpServletRequest req) {
		ModelAndView mav = new ModelAndView();
		int BookId = Integer.parseInt(req.getParameter("BookId"));
		System.out.println("book id is " + BookId);
		List<Books> listOfBooks = bookService.getAllBooksFromReadLater();
		req.setAttribute("Books", listOfBooks);
		mav.setViewName("readLater.jsp");
		return mav;
	}

}
