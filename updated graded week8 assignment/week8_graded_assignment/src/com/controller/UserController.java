
package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {

	@RequestMapping("/")
	public String start() {
		System.out.println("returning index page");
		return "index";
	}

	@RequestMapping("/index")
	public String index() {
		System.out.println("returning index page");
		return "index";
	}

	@RequestMapping("/registration")
	public String registration() {
		System.out.println("returning registration page");
		return "registration";
	}

}
