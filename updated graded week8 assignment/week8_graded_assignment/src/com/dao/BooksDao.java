package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.bean.Books;


@Repository
public class BooksDao {
	JdbcTemplate template;

	public void setTemplate(JdbcTemplate template) {
		
		this.template = template;
	}


	public List<Books> getAllBooks() {
		String sql = "select * from books";
		return template.query(sql, new BooksRowMapper());
	}

	public String addLike(Books books) {
		String sql = "insert into Like(bookId,title,totalPages,rating,isbn) values(" + books.getBookId() + ", '"
				+ books.getTitle() + "', '" + books.getTotalPages() + " ," + books.getRating() + " ," + books.getIsbn()
				+ ")";
		template.update(sql);
		return "Added to Like";
	}

	public List<Books> getAllBooksFromLikeSection() {
		String sql = "select * from Like";
		
		return template.query(sql, new BooksRowMapper());
	}

	public String addRedLater(Books books) {
		String sql = "insert into readLater(bookId,title,totalPages,rating,isbn) values(" + books.getBookId() + ", "
				+ books.getTitle() + ", " + books.getTotalPages() + " ," + books.getRating() + " ," + books.getIsbn()
				+ ")";

		template.update(sql);
		return "Added to ReadLater";
	}
	

	public List<Books> getAllBooksFromReadLater() {
		String sql = "select * from ReadLater";
		return template.query(sql, new BooksRowMapper());
	}
}
