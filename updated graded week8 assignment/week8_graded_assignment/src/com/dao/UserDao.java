package com.dao;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;

import com.bean.Login;
import com.bean.User;

@Repository
public class UserDao {
	JdbcTemplate template;

	public void setTemplate(JdbcTemplate template) {
		this.template = template;
	}

	public int save(User user) {
		String sql = "insert into users(username,email,password) values('" + user.getName() + "','" + user.getEmail()
				+ "','" + user.getPassword() + "')";
		return template.update(sql);
	}

	public User validateUser(Login login) {

		String sql = "select * from users where username='" + login.getUsername() + "' and password='"
				+ login.getPassword() + "'";

		List<User> users = template.query(sql, new UserMapper());

		return users.size() > 0 ? users.get(0) : null;
	}

}
