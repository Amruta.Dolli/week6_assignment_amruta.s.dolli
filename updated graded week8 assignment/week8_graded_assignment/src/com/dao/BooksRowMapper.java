package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.bean.Books;

public class BooksRowMapper implements RowMapper<Books> {

	public Books mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub

		Books b = new Books();
		b.setBookId(rs.getInt(1));
		b.setTitle(rs.getString(2));
		b.setTotalPages(rs.getInt(3));
		b.setRating(rs.getFloat(4));
		b.setIsbn(rs.getInt(5));
		return b;
	}

}
