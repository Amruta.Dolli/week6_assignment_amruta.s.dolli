package com.bean;

public class Books {

	int bookId;
	String title;
	int totalPages;
	float rating;
	int isbn;

	public Books() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Books(int bookId, String title, int totalPages, float rating, int isbn) {
		super();
		this.bookId = bookId;
		this.title = title;
		this.totalPages = totalPages;
		this.rating = rating;
		this.isbn = isbn;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public int getIsbn() {
		return isbn;
	}

	public void setIsbn(int isbn) {
		this.isbn = isbn;
	}

	@Override
	public String toString() {
		return "Books [bookId=" + bookId + ", title=" + title + ", totalPages=" + totalPages + ", rating=" + rating
				+ ", isbn=" + isbn + "]";
	}

}
