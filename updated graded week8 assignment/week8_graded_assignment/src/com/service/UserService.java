package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Login;
import com.bean.User;
import com.dao.UserDao;

@Service
public class UserService {
	@Autowired
	UserDao userDao;

	public int save(User user) {
		return userDao.save(user);
	}

	public User validateUser(Login login) {
		return userDao.validateUser(login);

	}
}
