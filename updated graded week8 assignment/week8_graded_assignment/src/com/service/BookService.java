package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Books;
import com.dao.BooksDao;

@Service
public class BookService {

	@Autowired
	BooksDao booksDao;

	public List<Books> getBooks() {
		return booksDao.getAllBooks();
	}

	public String addLike(Books books) {
		return booksDao.addLike(books);
	}

	public List<Books> getAllBooksFromLikeSection() {
		return booksDao.getAllBooksFromLikeSection();
	}

	public List<Books> addReadLater(Books books) {
		return booksDao.getAllBooksFromLikeSection();
	}

	public List<Books> getAllBooksFromReadLater() {
		return booksDao.getAllBooksFromLikeSection();
	}

}
