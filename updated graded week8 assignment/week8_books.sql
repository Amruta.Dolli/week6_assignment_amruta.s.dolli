create database  week8_books;
use week8_books;
CREATE TABLE books(
  bookId        INT NOT NULL, 
  title          VARCHAR(255) NOT NULL, 
  totalPages    INT NULL, 
  rating         DECIMAL(4, 2) NULL, 
  isbn           int not NULL, 
  PRIMARY KEY(bookId)
);

INSERT INTO BOOKS VALUES(1,"Java",300,4.5,145415),
(2,"Data Structure And Algorithm",450,4.5,452451),
(3,"Database System Concepts",350,3.5,145451),
(4,"Compiler Design",250,4.6,142451),
(5,"Pattern Classification",600,4.1,152451),
(6,"Fundamental Database System",450,4.2,222451),
(7,"Structured Computer Organization",220,4.3,111451),
(8,"Operating System",340,4.4,456451),
(9,"Microprocessor",680,4.6,145789),
(10,"Digital Computer",740,4.5,452151),
(11,"Theory Of Computation",620,4.7,452451),
(12,"Data Mining And Wearhousing",630,4.8,512451),
(13,"Artificial Neural Networks",800,4.9,112451),
(14,"Computer Vision And Programming",700,4.5,752451),
(15,"Image Processing",400,4.5,652451),
(16,"Computer Organizasion",500,4.1,952451),
(17,"Fundamentals of Eletrical",560,4.5,852451),
(18,"Basic Eletronics",360,3.5,882451),
(19,"Web Technology",840,3.6,772451),
(20,"Micro-controller",530,3.8,442451),
(21,"Advance Java",640,4.5,332451);

select * from books;

create table user (
   id 	   int not null auto_increment,
   name        varchar(45) not null,
   email 	   varchar(45) not null,
   password    varchar(45) not null,
   primary key(id)
);

insert into user values(1,"admin","admin@gmail.com","123456");

select * from user;

create table like(likedBookId int not null,userName varchar(25),bookId int,title varchar(25),totalPages int,rating float,isbn int,primary key(likedBookId));

insert into like values(3,"admin",1,"java",110,4,456);

select * from like;
create table ReadLater( readId int ,userName varchar(25), bookId int,title varchar(25),totalPages int,rating float,isbn int,primary key(readId));

insert into ReadLater values(2,"amruta",2,"Data Structure And Algorithm",450,4.5,452451);
insert into ReadLater values(5,"amruta",5,"Pattern Classification",600,4.1,152451);
insert into ReadLater values(8,"amruta",8,"Operating System",340,4.4,456451);



select * from ReadLater;