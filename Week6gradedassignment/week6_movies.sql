create database movies;
use movies;



create table Movies_Coming(ID int,Title varchar(20),Year int,Genre varchar(20),Ratings int,Category varchar(20));


create table Movies_inTheatres(ID int,Title varchar(20),Year int,Genre varchar(20),Ratings int,Category varchar(20));

create table Top_RatedIndia(ID int,Title varchar(20),Year int,Genre varchar(20),Ratings int,Category varchar(20));

create table Top_RatedMovies (ID int,Title varchar(20),Year int,Genre varchar(20),Ratings int,Category varchar(20));


insert into Movies_Coming values(1,'pushpa',2021,'Action',9,'Movies Coming');
insert into Movies_Coming values(2,'jaibhim',2021,'biopic',10,'Movies Coming');
insert into Movies_coming values(3,'valimai',2022,'Action',8,'Movies Coming');
insert into Movies_coming values(4,'game night',2022,'Action',7,'Movies Coming');
insert into Movies_coming values(5,'flower',2022,'Drama',8,'Movies Coming');




insert into Movies_inTheatres values(1,'Black Panther',2018,'Action',8,'Movies in Theatres');
insert into Movies_inTheatres values(2,'jersy',2019,'Biopic',9,'Movies in Theatres');
insert into Movies_inTheatres values(3,'Aiyaary',2018,'Thriller',6,'Movies in Theatres');
insert into Movies_inTheatres values(4,'Rajkumar',2019,'Drama',10,'Movies in Theatres');
insert into Movies_inTheatres values(5,'madgaja',2021,'Action',7,'Movies in Theatres');




insert into Top_RatedIndia values(1,'Anand',1971,'Drama',9,'Top Rated India');
insert into Top_RatedIndia values(2,'Dangal',2016,'Action',8,'Top Rated India');
insert into Top_RatedIndia values(3,'Drishyam',2013,'Thriller',10,'Top Rated India');
insert into Top_RatedIndia values(4,'Robert',2020,'Drama',9,'Top Rated India');
insert into Top_RatedIndia values(5,'Kotigibba',2019,'Thriller',9,'Top Rated India');


insert into Top_RatedMovies values(1,'Baazigar',1993,'Thriller',9,'Top Rated Movies');
insert into Top_RatedMovies values(2,'24',2016,'Action',10,'Top Rated Movies');
insert into Top_RatedMovies values(3,'Jodhaa Akbar',2008,'Drama',9,'Top Rated Movies');
insert into Top_RatedMovies values(4,'Joker',2016,'Drama',8,'Top Rated Movies');
insert into Top_RatedMovies values(5,'3idiots',2008,'Comedy',9,'Top Rated Movies');


select*from Movies_Coming;
select*from Top_RatedIndia;
select*from Top_RatedMovies;
select*from Movies_inTheatres;