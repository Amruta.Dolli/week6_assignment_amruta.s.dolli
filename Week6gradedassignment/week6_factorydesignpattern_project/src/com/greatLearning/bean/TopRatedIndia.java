package com.greatLearning.bean;

public class TopRatedIndia {
	int id;
	String title;
	int year;
	String genre;
	int ratings;
	String category;

	public TopRatedIndia() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TopRatedIndia(int id, String title, int year, String genre, int ratings, String category) {
		super();
		this.id = id;
		this.title = title;
		this.year = year;
		this.genre = genre;
		this.ratings = ratings;
		this.category = category;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public int getRatings() {
		return ratings;
	}

	public void setRatings(int ratings) {
		this.ratings = ratings;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "TopRatedIndia [id=" + id + ", title=" + title + ", year=" + year + ", genre=" + genre + ", ratings="
				+ ratings + ", category=" + category + "]";
	}

}
