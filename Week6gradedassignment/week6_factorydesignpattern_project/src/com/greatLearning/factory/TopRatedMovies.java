package com.greatLearning.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class TopRatedMovies implements IMovies {
	@Override
	public void display() throws Exception {
		// TODO Auto-generated method stub
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/movies", "root", "1234");

		Statement s = con.createStatement();
		ResultSet resset = s.executeQuery("select * from Top_RatedMovies ");
		while (resset.next())
			System.out.println(resset.getInt(1) + ",  " + resset.getString(2) + ", " + resset.getInt(3) + ", "
					+ resset.getString(4) + ", " + resset.getInt(5) + ", " + resset.getString(6));
		con.close();
	}
}
