package com.greatLearning.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class TheatricalRelease implements IMovies {
	@Override
	public void display() throws Exception {
		// TODO Auto-generated method stub
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/movies", "root", "1234");
		Statement s = con.createStatement();
		ResultSet rs = s.executeQuery("select * from Movies_inTheatres ");
		while (rs.next())
			System.out.println(rs.getInt(1) + ",  " + rs.getString(2) + ",  " + rs.getInt(3) + ", " + rs.getString(4)
					+ ", " + rs.getInt(5) + ", " + rs.getString(6));
		con.close();
	}

}
