package com.greatLearning.factory;

public class FactoryPatternType {
	public IMovies getMovies(String movieType) {

		if (movieType == null) {
			return null;
		}
		if (movieType.equalsIgnoreCase("MOVIES_COMING")) {
			return new ComingSoon();
		} else if (movieType.equalsIgnoreCase("MOVIES_IN_THEATRES")) {
			return new TheatricalRelease();
		} else if (movieType.equalsIgnoreCase("TOP_RATED_INDIA")) {
			return new TopRatedMoviesInIndia();
		} else if (movieType.equalsIgnoreCase("TOP_RATED_MOVIES")) {
			return new TopRatedMovies();
		}
		return null;
	}

}
