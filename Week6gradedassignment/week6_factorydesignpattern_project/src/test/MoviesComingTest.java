package test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatLearning.bean.MoviesComing;

public class MoviesComingTest {

	@Test
	public void testGetId() {
		MoviesComing mv = new MoviesComing();
		mv.setId(1);
		assertTrue(mv.getId() == 1);
		// fail("Not yet implemented");
	}

	@Test
	public void testSetId() {
		MoviesComing mv = new MoviesComing();
		mv.setId(1);
		assertTrue(mv.getId() == 1);
		// fail("Not yet implemented");
	}

	@Test
	public void testGetTitle() {
		MoviesComing mv = new MoviesComing();
		mv.setTitle("puspa");
		assertTrue(mv.getTitle() == "puspa");
		// fail("Not yet implemented");
	}

	@Test
	public void testSetTitle() {
		// fail("Not yet implemented");
		MoviesComing mv = new MoviesComing();
		mv.setTitle("puspa");
		assertTrue(mv.getTitle() == "puspa");
	}

	@Test
	public void testGetYear() {
		MoviesComing mv = new MoviesComing();
		mv.setYear(2021);
		assertTrue(mv.getYear() == 2021);
		// fail("Not yet implemented");
	}

	@Test
	public void testSetYear() {
		MoviesComing mv = new MoviesComing();
		mv.setYear(2021);
		assertTrue(mv.getYear() == 2021);
		// fail("Not yet implemented");
	}

	@Test
	public void testGetGenre() {
		MoviesComing mv = new MoviesComing();
		mv.setGenre("Action");
		assertTrue(mv.getGenre() == "Action");
		// fail("Not yet implemented");
	}

	@Test
	public void testSetGenre() {
		MoviesComing mv = new MoviesComing();
		mv.setGenre("Action");
		assertTrue(mv.getGenre() == "Action");
		// fail("Not yet implemented");
	}

	@Test
	public void testGetRatings() {
		MoviesComing mv = new MoviesComing();
		mv.setRatings(9);
		assertTrue(mv.getRatings() == 9);
		// fail("Not yet implemented");
	}

	@Test
	public void testSetRatings() {
		MoviesComing mv = new MoviesComing();
		mv.setRatings(9);
		assertTrue(mv.getRatings() == 9);
		// fail("Not yet implemented");
	}

	@Test
	public void testGetCategory() {
		MoviesComing mv = new MoviesComing();
		mv.setCategory("Movies Coming");
		assertTrue(mv.getCategory() == "Movies Coming");
		// fail("Not yet implemented");
	}

	@Test
	public void testSetCategory() {
		MoviesComing mv = new MoviesComing();
		mv.setCategory("Movies Coming");
		assertTrue(mv.getCategory() == "Movies Coming");
		// fail("Not yet implemented");
	}

	// @Test
	public void testToString() {
		// fail("Not yet implemented");
	}

}
