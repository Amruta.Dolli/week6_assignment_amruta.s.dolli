package test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatLearning.bean.MoviesInTheaters;

public class MoviesInTheatersTest {

	@Test
	public void testGetId() {
		MoviesInTheaters mt = new MoviesInTheaters();
		mt.setId(2);
		assertTrue(mt.getId() == 2);
		// fail("Not yet implemented");
	}

	@Test
	public void testSetId() {
		MoviesInTheaters mt = new MoviesInTheaters();
		mt.setId(2);
		assertTrue(mt.getId() == 2);
		// fail("Not yet implemented");
	}

	@Test
	public void testGetTitle() {
		MoviesInTheaters mt = new MoviesInTheaters();
		mt.setTitle("jersey");
		assertTrue(mt.getTitle() == "jersey");
		// fail("Not yet implemented");
	}

	@Test
	public void testSetTitle() {
		MoviesInTheaters mt = new MoviesInTheaters();
		mt.setTitle("jersey");
		assertTrue(mt.getTitle() == "jersey");
		// fail("Not yet implemented");
	}

	@Test
	public void testGetYear() {
		MoviesInTheaters mt = new MoviesInTheaters();
		mt.setYear(2019);
		assertTrue(mt.getYear() == 2019);
		// fail("Not yet implemented");
	}

	@Test
	public void testSetYear() {
		MoviesInTheaters mt = new MoviesInTheaters();
		mt.setYear(2019);
		assertTrue(mt.getYear() == 2019);
		// fail("Not yet implemented");
	}

	@Test
	public void testGetGenre() {
		MoviesInTheaters mt = new MoviesInTheaters();
		mt.setGenre("biopic");
		assertTrue(mt.getGenre() == "biopic");
		// fail("Not yet implemented");
	}

	@Test
	public void testSetGenre() {
		MoviesInTheaters mt = new MoviesInTheaters();
		mt.setGenre("biopic");
		assertTrue(mt.getGenre() == "biopic");
		// fail("Not yet implemented");
	}

	@Test
	public void testGetRatings() {
		MoviesInTheaters mt = new MoviesInTheaters();
		mt.setRatings(9);
		assertTrue(mt.getRatings() == 9);
		// fail("Not yet implemented");
	}

	@Test
	public void testSetRatings() {
		MoviesInTheaters mt = new MoviesInTheaters();
		mt.setRatings(9);
		assertTrue(mt.getRatings() == 9);
		// fail("Not yet implemented");
	}

	@Test
	public void testGetCategory() {
		MoviesInTheaters mt = new MoviesInTheaters();
		mt.setCategory("Movies in Theaters");
		assertTrue(mt.getCategory() == "Movies in Theaters");
		// fail("Not yet implemented");
	}

	@Test
	public void testSetCategory() {
		MoviesInTheaters mt = new MoviesInTheaters();
		mt.setCategory("Movies in Theaters");
		assertTrue(mt.getCategory() == "Movies in Theaters");
		// fail("Not yet implemented");
	}

	// @Test
	public void testToString() {
		// fail("Not yet implemented");
	}

}
