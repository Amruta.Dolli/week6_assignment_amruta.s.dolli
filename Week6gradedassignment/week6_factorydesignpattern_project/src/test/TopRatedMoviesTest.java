
package test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatLearning.bean.TopRatedMovies;

public class TopRatedMoviesTest {

	@Test
	public void testGetId() {
		TopRatedMovies rm = new TopRatedMovies();
		rm.setId(4);
		assertTrue(rm.getId() == 4);
		// fail("Not yet implemented");
	}

	@Test
	public void testSetId() {
		TopRatedMovies rm = new TopRatedMovies();
		rm.setId(4);
		assertTrue(rm.getId() == 4);
		// fail("Not yet implemented");
	}

	@Test
	public void testGetTitle() {
		TopRatedMovies rm = new TopRatedMovies();
		rm.setTitle("Joker");
		assertTrue(rm.getTitle() == "Joker");
		// fail("Not yet implemented");
	}

	@Test
	public void testSetTitle() {
		TopRatedMovies rm = new TopRatedMovies();
		rm.setTitle("Joker");
		assertTrue(rm.getTitle() == "Joker");
		// fail("Not yet implemented");
	}

	@Test
	public void testGetYear() {
		TopRatedMovies rm = new TopRatedMovies();
		rm.setYear(2016);
		assertTrue(rm.getYear() == 2016);
		// fail("Not yet implemented");
	}

	@Test
	public void testSetYear() {
		TopRatedMovies rm = new TopRatedMovies();
		rm.setYear(2016);
		assertTrue(rm.getYear() == 2016);
		// fail("Not yet implemented");
	}

	@Test
	public void testGetGenre() {
		TopRatedMovies rm = new TopRatedMovies();
		rm.setGenre("Drama");
		assertTrue(rm.getGenre() == "Drama");
		// fail("Not yet implemented");
	}

	@Test
	public void testSetGenre() {
		TopRatedMovies rm = new TopRatedMovies();
		rm.setGenre("Drama");
		assertTrue(rm.getGenre() == "Drama");
		// fail("Not yet implemented");
	}

	@Test
	public void testGetRatings() {
		TopRatedMovies rm = new TopRatedMovies();
		rm.setRatings(8);
		assertTrue(rm.getRatings() == 8);
		// fail("Not yet implemented");
	}

	@Test
	public void testSetRatings() {
		TopRatedMovies rm = new TopRatedMovies();
		rm.setRatings(8);
		assertTrue(rm.getRatings() == 8);
		// fail("Not yet implemented");
	}

	@Test
	public void testGetCategory() {
		TopRatedMovies rm = new TopRatedMovies();
		rm.setCategory("Top Rated Movies");
		assertTrue(rm.getCategory() == "Top Rated Movies");
		// fail("Not yet implemented");
	}

	@Test
	public void testSetCategory() {
		TopRatedMovies rm = new TopRatedMovies();
		rm.setCategory("Top Rated Movies");
		assertTrue(rm.getCategory() == "Top Rated Movies");
		// fail("Not yet implemented");
	}

	// @Test
	public void testToString() {
		// fail("Not yet implemented");
	}

}
