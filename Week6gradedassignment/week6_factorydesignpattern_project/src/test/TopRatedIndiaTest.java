package test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatLearning.bean.TopRatedIndia;

public class TopRatedIndiaTest {

	@Test
	public void testGetId() {
		TopRatedIndia tri = new TopRatedIndia();
		tri.setId(3);
		assertTrue(tri.getId() == 3);
		// fail("Not yet implemented");
	}

	@Test
	public void testSetId() {
		TopRatedIndia tri = new TopRatedIndia();
		tri.setId(3);
		assertTrue(tri.getId() == 3);
		// fail("Not yet implemented");
	}

	@Test
	public void testGetTitle() {
		TopRatedIndia tri = new TopRatedIndia();
		tri.setTitle("drishyam");
		assertTrue(tri.getTitle() == "drishyam");
		// fail("Not yet implemented");
	}

	@Test
	public void testSetTitle() {
		TopRatedIndia tri = new TopRatedIndia();
		tri.setTitle("drishyam");
		assertTrue(tri.getTitle() == "drishyam");
		// fail("Not yet implemented");
	}

	@Test
	public void testGetYear() {
		TopRatedIndia tri = new TopRatedIndia();
		tri.setYear(2013);
		assertTrue(tri.getYear() == 2013);
		// fail("Not yet implemented");
	}

	@Test
	public void testSetYear() {
		TopRatedIndia tri = new TopRatedIndia();
		tri.setYear(2013);
		assertTrue(tri.getYear() == 2013);
		// fail("Not yet implemented");
	}

	@Test
	public void testGetGenre() {
		TopRatedIndia tri = new TopRatedIndia();
		tri.setGenre("Thriller");
		assertTrue(tri.getGenre() == "Thriller");
		// fail("Not yet implemented");
	}

	@Test
	public void testSetGenre() {
		TopRatedIndia tri = new TopRatedIndia();
		tri.setGenre("Thriller");
		assertTrue(tri.getGenre() == "Thriller");
		// fail("Not yet implemented");
	}

	@Test
	public void testGetRatings() {
		TopRatedIndia tri = new TopRatedIndia();
		tri.setRatings(10);
		assertTrue(tri.getRatings() == 10);
		// fail("Not yet implemented");
	}

	@Test
	public void testSetRatings() {
		TopRatedIndia tri = new TopRatedIndia();
		tri.setRatings(10);
		assertTrue(tri.getRatings() == 10);
		// fail("Not yet implemented");
	}

	@Test
	public void testGetCategory() {
		TopRatedIndia tri = new TopRatedIndia();
		tri.setCategory("Top Rated India");
		assertTrue(tri.getCategory() == "Top Rated India");
		// fail("Not yet implemented");
	}

	@Test
	public void testSetCategory() {
		TopRatedIndia tri = new TopRatedIndia();
		tri.setCategory("Top Rated India");
		assertTrue(tri.getCategory() == "Top Rated India");
		// fail("Not yet implemented");
	}

	// @Test
	public void testToString() {
		// fail("Not yet implemented");
	}

}
