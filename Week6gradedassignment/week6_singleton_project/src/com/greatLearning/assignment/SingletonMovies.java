package com.greatLearning.assignment;

import java.sql.*;

public class SingletonMovies {
	public static void main(String[] args) {

		Connection conn = SingletonPatternMovies.getSingletonMovies();
		try {
			String qurey = "select * from Top_RatedIndia";
			Statement statement = conn.createStatement();
			ResultSet rSet = statement.executeQuery(qurey);
			while (rSet.next()) {
				System.out.println(rSet.getInt(1) + ", " + rSet.getString(2) + ", " + rSet.getInt(3) + ", "
						+ rSet.getString(4) + ", " + rSet.getInt(5) + ", " + rSet.getNString(6));
			}

			System.out.println("\n");
			String qurey1 = "select * from movies_coming";
			Statement statement1 = conn.createStatement();
			ResultSet rSet1 = statement1.executeQuery(qurey1);
			while (rSet1.next()) {
				System.out.println(rSet1.getInt(1) + ",  " + rSet1.getString(2) + ",  " + rSet1.getInt(3) + ",  "
						+ rSet1.getString(4) + ",  " + rSet1.getInt(5) + ",  " + rSet1.getNString(6));
			}

			System.out.println("\n");
			String qurey2 = "select * from Movies_inTheatres";

			Statement statement2 = conn.createStatement();
			ResultSet rSet2 = statement2.executeQuery(qurey2);
			while (rSet2.next()) {
				System.out.println(rSet2.getInt(1) + ",  " + rSet2.getString(2) + ",  " + rSet2.getInt(3) + ", "
						+ rSet2.getString(4) + ", " + rSet2.getInt(5) + ", " + rSet2.getNString(6));
			}

			System.out.println("\n");
			String qurey3 = "select * from  Top_RatedMovies";
			Statement statement3 = conn.createStatement();
			ResultSet rSet3 = statement3.executeQuery(qurey3);
			while (rSet3.next()) {
				System.out.println(rSet3.getInt(1) + ", " + rSet3.getString(2) + ",  " + rSet3.getInt(3) + ",  "
						+ rSet3.getString(4) + ", " + rSet3.getString(5) + ",  " + rSet3.getNString(6));
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
