package com.greatLearning.assignment;

import java.sql.*;

public class SingletonPatternMovies {
	private static Connection conn = null;

//	private SingletonPatternMovies() {
//
//	}

	public static Connection getSingletonMovies() {
		try {
			if (conn == null) {
				Class.forName("com.mysql.cj.jdbc.Driver");
				conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/movies", "root", "1234");
				System.out.println("Database connected successfully\n");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

}
