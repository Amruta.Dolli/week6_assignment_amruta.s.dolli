package test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Connection;

import org.junit.jupiter.api.Test;

import com.greatLearning.assignment.SingletonPatternMovies;

import junit.framework.Assert;

class SingletonPatternMoviesTest {

	@Test
	void testGetSingletonMovies() throws Exception {
		Connection con = SingletonPatternMovies.getSingletonMovies();
		Assert.assertNotNull(con);
		Assert.assertTrue(con.isValid(0));
		con.close();

		// fail("Not yet implemented");
	}

}
